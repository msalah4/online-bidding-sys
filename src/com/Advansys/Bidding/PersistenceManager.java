 package com.Advansys.Bidding;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public enum PersistenceManager {

	INSTANCE;

	private EntityManagerFactory entityManagerFactory;

	private PersistenceManager() {
		// "org.hibernate.tutorial.jpa" was the value of the name attribute of
		// the persistence-unit element.
		// Get instance of the "EntityManagerFactory" via the Hibernate JPA
		// implementation
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa");
		System.out.println("Finished");
	}

	public EntityManager getEntityManager() {
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa");
		return entityManagerFactory.createEntityManager();
	}

	public void close() {
		entityManagerFactory.close();
	}
}
