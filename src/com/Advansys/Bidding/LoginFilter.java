package com.Advansys.Bidding;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



@WebFilter("/authFilter")
public class LoginFilter  implements Filter {


	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest)arg0;
		
		HttpSession session = req.getSession(false);
		String uri = req.getRequestURI();
				
		if(session == null && !(uri.endsWith("html") || uri.endsWith("LoginServlet")))
		{
			
		}
		else
		{
			arg2.doFilter(arg0, arg1);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
		
	}

	
	@Override
	public void destroy() {
		
		
	}
}
